import java.io.InputStream;
import java.util.Scanner;

public class Hex {

	final byte N = 0;
	final byte NE = 1;
	final byte E = 2;
	final byte SE = 3;
	final byte S = 4;
	final byte SW = 5;
	final byte W = 6;
	final byte NW = 7;

	final byte JUMP_DP = 1;
	final byte JUMP_IP = 2;
	final byte STORE = 3;
	final byte ADD = 4;
	final byte IF = 5;
	final byte PRINT = 6;
	final byte TURN = 7;

	final byte HALT = 0;

	byte[][] grid;
//	int dpi, dpj, ipi, ipj = 0;
	byte dir = E;

	public static void main (String args[]) {
		new Hex().run(args[0]);
	}

	public void run(String progFile) {

		String prog = "";
		int lineCount = 0;

		try {
			InputStream is = getClass().getResourceAsStream(progFile);
			Scanner scanner = new Scanner(is);

			// loop through search terms
			while (scanner.hasNextLine()) {
				prog += scanner.nextLine().replaceAll("[^0-9a-fA-F]+","") + "\n";
				lineCount++;
			}

			scanner.close();
		} catch (Exception e) {
			System.out.println("Error reading input file");
			System.exit(1);
		}

		grid = new byte[lineCount][];
		int n = 0;

		for (String line : prog.split("\n")) {
			grid[n] = new byte[line.length()];
			for (int i = 0; i < grid[n].length; i++) {
				if (getByteFromHex(line.charAt(i)) == -1) {
					System.exit(1);
				}
				grid[n][i] = getByteFromHex(line.charAt(i));
			}
			n++;
		}

		evaluate();
	}

	public void evaluate() {
		Point ip = new Point();
		Point dp = new Point();
		boolean running = true;
		boolean skip = false;
		byte move;

		while (running) {
			//System.out.print(getHexFromByte(grid[ipi][ipj])+""+getHexFromByte(grid[ipi][ipj+1]));
			//System.out.println(" | "+grid[dpi][dpj]);

			switch (getVal(ip) % 8) {
				case JUMP_DP: move = 3; break;
				case JUMP_IP: move = (byte) (skip ? 3 : 0); break;
				default: move = 2; break;
			}

			// do instruction
			if (!skip) {
				byte a1 = getVal(look(ip, dir));
				byte a2 = getVal(look(look(ip, dir), dir));
				switch (getVal(ip) % 8) {
					case JUMP_DP:
						dp = move(dp, a1, a2);
						break;
					case JUMP_IP:
						ip = move(ip, a1, a2);
						break;
					case STORE:
						setVal(dp, a1);
						break;
					case ADD:
						setVal(dp, (byte) ((getVal(dp) + a1) % 0xf));
						break;
					case IF:
						if (getVal(dp) != a1) {
							skip = true;
						}
						break;
					case PRINT:
						printChar(dp, a1);
						break;
					case TURN:
						dir = a1;
						break;
					case HALT:
						if (a1 % 8 == 0) {
							running = false;
						}
						break;
					default:
						break;
				}
			} else {
				skip = false;
			}

			ip = move(ip, dir, move);
		}

		System.out.println("\nProgram terminated");
		System.exit(0);
	}

	private void printChar(Point p, byte dir) {
		char c = (char) (getVal(p) * 0x10);
		c += getVal(look(p, dir));
		System.out.print(c);
	}

	public void printGrid() {
		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				System.out.print(getHexFromByte(grid[i][j]));
			}
			System.out.println();
		}
	}

	public char getHexFromByte(byte b) {
		if (b < 10) {
			return (char)(b + 0x30);
		}
		return (char)(b + 0x37);
	}

	public byte getByteFromHex(char c) {
		if (c >= 0x41 && c <= 0x5a) {
			return (byte)(c - 0x37);
		}
		if (c >= 0x61 && c <= 0x7a) {
			return (byte)(c - 0x57);
		}
		if (c >= 0x30 && c <= 0x39) {
			return (byte)(c - 0x30);
		}
		return -1;
	}

	public byte getVal(Point p) {
		return grid[p.r][p.c];
	}

	public void setVal(Point p, byte val) {
		grid[p.r][p.c] = val;
	}

	public Point move(Point p, byte dir, byte dist) {
		Point np = p;
		for (; dist > 0; dist--) {
			np = look(np, dir);
		}
		return np;
	}

	public Point look(Point p, byte d) {
		switch (d % 8) {
			case N : return n(p);
			case S : return s(p);
			case E : return e(p);
			case W : return w(p);
			case NE : return ne(p);
			case NW : return nw(p);
			case SE : return se(p);
			case SW : return sw(p);
			default: return p;
		}
	}

	public Point n(Point p) {
		int nr = p.r;
		do {
			nr--;
			if (nr < 0) {
				nr += grid.length;
			}
		} while (grid[nr].length <= p.c);
		return new Point(nr, p.c);
	}

	public Point s(Point p) {
		int nr = p.r;
		do {
			nr++;
			if (nr >= grid.length) {
				nr = 0;
			}
		} while (grid[nr].length <= p.c);
		return new Point(nr, p.c);
	}

	public Point e(Point p) {
		int nc = p.c + 1;
		if (nc >= grid[p.r].length) {
			nc = 0;
		}
		return new Point(p.r, nc);
	}

	public Point w(Point p) {
		int nc = p.c - 1;
		if (nc < 0) {
			nc = grid[p.r].length - 1;
		}
		return new Point(p.r, nc);
	}

	public Point nw(Point p) {
		return w(n(p));
	}

	public Point ne(Point p) {
		return e(n(p));
	}

	public Point sw(Point p) {
		return w(s(p));
	}

	public Point se(Point p) {
		return e(s(p));
	}

	class Point {
		public int r;
		public int c;

		public Point() {}

		public Point(int r, int c) {
			this.r = r;
			this.c = c;
		}
	}
}

