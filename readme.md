#HEX
----

Hex is an interpreted programming language consisting exclusively of hexadecimal characters.

##Specification
---

Hex is written as a two dimensional block of hexadecimal characters, which comprise both the instructions and the data.  The language is processed using two separate pointers, one for data and one for the next instruction.  Because these pointers operate on the exact same set of characters, it is easily possible to write self-modifying code.  Any non-hex characters and spaces are ignored, including positionally.

Many instructions have a direction element to them.  Directions are referred to using cardinal compass directions.  They are defined starting with north (N) represented by 0, and continuing around the compass in a clockwise direction.  Each direction (and instruction) is determined by taking the value at the relevant pointer mod 8, so 0 and 8, 1 and 9, etc. are functionally equivalent in instructions.

**Directions:**

```
{0|8} = N  
{1|9} = NE  
{2|a} = E  
{3|b} = SE  
{4|c} = S  
{5|d} = SW  
{6|e} = W  
{7|f} = NW  
```

Processing begins with both the data pointer (DP) and the instruction pointer (IP) located at (0,0) and the processing direction (DIR) defaulting to E.  Each instruction consists of two or three hexadecimal digits, which are read starting at IP, and moving in DIR.  After each instruction has been processed, the instruction pointer is moved in DIR to the next digit immediately following the instruction.
  
When pointers reach the end of the row or column they are on, they will wrap around to the opposite end.  This will happen even in the middle of an instruction.  If the pointer is moving diagonally, and it will go off the end of both the row and column at the same time, the row wrapping is processed before the column wrapping.  This matches the way compass directions are used, for example if we move a pointer SE, it will go S first, then E.  Rows in a program need not be the same length.  If the first row is 10 digits long, and the second is 5 digits long, then wrapping from the first row at the sixth digit will simply wrap around again to itself.  More precisely, when a pointer is moved N or S, it looks in that direction until the next row with an adequate number of digits is found, wrapping when necessary.

**Instructions:**

```
+---------------+----------------------------------------------------------+------------------------------------+
|     Instr     |                       Description                        |               Notes                |
+---------------+----------------------------------------------------------+------------------------------------+
| {0|8} {0|8}   | end program                                              |                                    |
| {1|9} {D} {Y} | move DP in D direction Y spaces                          |                                    |
| {2|a} {D} {Y} | move IP in D direction Y spaces                          |                                    |
| {3|b} {Y}     | store Y as value at DP                                   |                                    |
| {4|c} {Y}     | add Y to value at DP                                     | this can cause overflow/wraparound |
| {5|d} {Y}     | if value at DP == Y, then execute next instr., else skip |                                    |
| {6|e} {D}     | print ascii value at DP in D direction                   |                                    |
| {7|f} {D}     | change execution direction to D                          |                                    |
+---------------+----------------------------------------------------------+------------------------------------+
```


**Simple Hello World program:**  
```
941 50 22b 6a 122 26a 00  
48 65 6c 6c 6f 20 77 6f 72 6c 64 21 0
```